#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "openacc.h"
#include <mpi.h>

#ifndef M_PI
#define M_PI           3.14159265358979323846
#endif

#define N 256
const float L = 64;
const int Nt = 100000000;
const float dx = L / N;
const float dt = 0.2;
const float mm = 0.3;
const int newton_max = 1000;
const float newton_dev = 1e-3;
const int ana_dt = 100000;

// Forward declaration of wrapper function that will call CUFFT
extern void launchCUFFT(float *data, int n, void *stream);
extern void launchiCUFFT(float *data, int n, void *stream);

void calc_im(float * f, int n)
{
    float sum = 0.;
    #pragma acc parallel loop present(f[0:2*n]) reduction(+:sum)
    for(int i = 0; i < 2 * n; i += 2)
    {
        sum += fabsf(f[i + 1]);
    }
    printf("im=%f\n", sum);
}

int index(int x, int y, int z)
{
    return z + N * (y + N * x);
}

int main(int argc, char *argv[])
{
    // Initialize the MPI environment
    MPI_Init(NULL, NULL);

    // Get the number of processes
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    // Get the rank of the process
    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

    if(argc != 2 && world_rank == 0)
    {
        printf("USAGE: ./run input.dat\n");
	return -1;
    }

    FILE * f = fopen(argv[1], "r");
    int n_tasks;
    fscanf(f, "n_tasks=%d\n", &n_tasks);

    if(n_tasks != world_size && world_rank == 0)
    {
        printf("number of tasks (%d) not equal world size (%d).\n", n_tasks, world_size);
    }

    float a, epsilon, period;

    for(int i = 0; i < n_tasks; i++)
    {
	fscanf(f, "a=%g,epsilon=%g,period=%g\n", &a, &epsilon, &period);
	if(world_rank == i)
	{
	    break;
	}
    }

    fclose(f);

    
    printf("[%d] a=%g,epsilon=%g,period=%g\n", world_rank, a, epsilon, period);

    const int n = N * N * N;
    float *m = malloc(2 * n *sizeof(float));
    float *m_new = malloc(2 * n *sizeof(float));
    float *m3 = malloc(2 * n *sizeof(float));
    float *m3_new = malloc(2 * n *sizeof(float));

    srand(time(NULL));

    // initialize m with random field
    float mbar = 0.;

    for(int i = 0; i < 2 * n; i += 2)
    {
        m[i] = 0.02 * (rand() / (float) RAND_MAX - 0.5);
        m[i + 1] = 0.;
        mbar += m[i];
    }

    mbar /= n;

    for(int i = 0; i < 2 * n; i += 2)
    {
        m[i] += mm - mbar;
    }

    // Copy data to device at start of region and back to host and end of region
#pragma acc enter data copyin(m[0:2*n],m_new[0:2*n],m3[0:2*n],m3_new[0:2*n])

    int newton_avg = 0;

    for(int t = 0; t < Nt; t++)
    {
        const float alpha1 = 1 + epsilon * sin(2 * M_PI * t * dt / period);
        const float alpha2 = 1 + epsilon * sin(2 * M_PI * (t + 1) * dt / period);

        // initial guess for new m; calculate m3
#pragma acc parallel loop present(m[0:2*n],m_new[0:2*n],m3_new[0:2*n])
        for(int i = 0; i < 2 * n; i++)
        {
            m_new[i] = m[i];
            m3[i] = m[i] * m[i] * m[i];
        }

        // calculate transforms of m, m3
#pragma acc host_data use_device(m,m3)
        {
            void *stream = acc_get_cuda_stream(acc_async_sync);
            launchCUFFT(m, N, stream);
            launchCUFFT(m3, N, stream);
        }

        // Newton loop
        float dev = n;
        int newton;
        for(newton = 0; newton < newton_max && dev > newton_dev; newton++)
        {
            // calculate m3_new
#pragma acc parallel loop present(m_new[0:2*n],m3_new[0:2*n])
            for(int i = 0; i < 2 * n;  i++)
            {
                m3_new[i] = m_new[i] * m_new[i] * m_new[i];
            }

            // calculate transforms of m_new, m3_new
#pragma acc host_data use_device(m_new,m3_new)
            {
                void *stream = acc_get_cuda_stream(acc_async_sync);
                launchCUFFT(m_new, N, stream);
                launchCUFFT(m3_new, N, stream);
            }

            // actual newton step
            dev = 0.;
#pragma acc parallel loop collapse(3) present(m[0:2*n],m_new[0:2*n],m3[0:2*n],m3_new[0:2*n]) reduction(max:dev)
            for(int x = 0; x < N; x++)
            {
                for(int y = 0; y < N; y++)
                {
                    for(int z = 0; z < N; z++)
                    {
                        if(x + y + z > 0)
                        {
                            const int i = 2 * index(x, y, z);
                            float kx, ky, kz;

                            if(x <= N / 2)
                            {
                                kx = x;
                            }
                            else
                            {
                                kx = x - N;
                            }


                            if(y <= N / 2)
                            {
                                ky = y;
                            }
                            else
                            {
                                ky = y - N;
                            }


                            if(z <= N / 2)
                            {
                                kz = z;
                            }
                            else
                            {
                                kz = z - N;
                            }

                            const float k2 = 2 * M_PI * 2 * M_PI * (kx * kx + ky * ky + kz * kz) / (L * L);
                            const float rhs0 = 0.5 * (m_new[i] * (-a + alpha2 * k2 - k2 * k2) - k2 * m3_new[i] + m[i] * (-a + alpha1 * k2 - k2 * k2) - k2 * m3[i]) - (m_new[i] - m[i]) / dt;
                            const float rhs1 = 0.5 * (m_new[i + 1] * (-a + alpha2 * k2 - k2 * k2) - k2 * m3_new[i + 1] + m[i + 1] * (-a + alpha1 * k2 - k2 * k2) - k2 * m3[i + 1]) - (m_new[i + 1] - m[i + 1]) / dt;
                            m_new[i] -= rhs0 / (0.5 * (-a + alpha2 * k2 - k2 * k2) - 1. / dt);
                            m_new[i + 1] -= rhs1 / (0.5 * (-a + alpha2 * k2 - k2 * k2) - 1. / dt);
                            const float deviation = fabsf(rhs0) + fabsf(rhs1);
                            if(deviation > dev)
                            {
                                dev = deviation;
                            }
                        }
                        else
                        {
                            m_new[0] = mm * n;
                            m_new[1] = 0.;
                        }
                    }
                }
            }

	    dev /= n;

            // inverse transform for m_new
#pragma acc host_data use_device(m_new)
            {
                void *stream = acc_get_cuda_stream(acc_async_sync);
                launchiCUFFT(m_new, N, stream);
            }

            // need to normalize
#pragma acc parallel loop present(m_new[0:2*n])
            for(int i = 0; i < 2 * n; i += 2)
            {
                m_new[i] /= n;
		m_new[i + 1] = 0.;
            }

            //printf("newton = %d, dev = %f\n", newton, dev);
        }

	newton_avg += newton;

        // move m_new to m
#pragma acc parallel loop present(m[0:2*n],m_new[0:2*n])
        for(int i = 0; i < 2 * n; i ++)
        {
            m[i] = m_new[i];
        }

        if(newton == newton_max)
        {
            printf("[%d] newton not converged @ t = %d (dev = %f)\n", world_rank, t, dev);
            return -1;
        }
        //printf("newton = %d, dev = %lg\n", newton, dev / n);

        if(t % ana_dt == 0)
        {
            printf("[%d] t=%f; newton_avg=%f\n", world_rank, t * dt, newton_avg / (float) ana_dt);
            newton_avg = 0;

            char fname[1024];
            sprintf(fname, "m%g_a%g_epsilon%g_period%g_t%g.dat", m, a, epsilon, period, t * dt);
            FILE *f = fopen(fname, "w");

#pragma acc update host(m[0:2*n])

            for(int i = 0; i < 2 * n; i += 2)
            {
                fprintf(f, "%g ", m[i]);
            }

            fclose(f);
        }

    }

#pragma acc exit data copyout(m[0:2*n],m_new[0:2*n],m3[0:2*n],m3_new[0:2*n])

    free(m);
    free(m_new);
    free(m3);
    free(m3_new);

    // Finalize the MPI environment.
    MPI_Finalize();

    return 0;
}
