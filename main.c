#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <math.h>
#include "openacc.h"
#include <cufft.h>
#include <curand.h>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

const int newton_max = 10000;
const float newton_dev = 1e-0;

// Forward declaration of wrapper function that will call CUDA functions
extern void initCUFFT(int nx, int ny, int nz, void *stream);
extern void launchCUFFT(float * idata, float * odata, cufftType type);
extern void cleanCUFFT();
extern void initCURAND(void *stream);
extern void launchCURAND_normal(float *d_buffer, int num);
extern void launchCURAND_uniform(float *d_buffer, int num);
extern void cleanCURAND();

int index(int x, int y, int z, int Nx, int Ny, int Nz)
{
    return z + (Nz / 2 + 1) * (y + Ny * x);
}

float calc_freq(int x, int Nx, float Lx)
{
    if(x <= Nx / 2)
    {
        return 2 * M_PI / Lx * x;
    }
    else
    {
        return 2 * M_PI / Lx * (x - Nx);
    }
}

int main(int argc, char *argv[])
{
    const int argc_valid = 13;

    if(argc != argc_valid)
    {
        printf("USAGE: ./run Lx Ly Lz Nx Ny Nz dt steps dt_ana alpha name beta (%d / %d)\n", argc, argc_valid);
        return -1;
    }

    const float Lx = atof(argv[1]);
    const float Ly = atof(argv[2]);
    const float Lz = atof(argv[3]);
    const int Nx = atoi(argv[4]);
    const int Ny = atoi(argv[5]);
    const int Nz = atoi(argv[6]);
    const float dt = atof(argv[7]);
    const int steps = atoi(argv[8]);
    const int dt_ana = atoi(argv[9]);
    const float alpha = atof(argv[10]);
    const float beta = atof(argv[12]);

    printf("Lx=%lg Ly=%lg Lz=%lg Nx=%d Ny=%d Nz=%d dt=%lg steps=%d dt_ana=%d alpha=%lg name=%s beta=%lg\n", Lx, Ly, Lz, Nx, Ny, Nz, dt, steps, dt_ana, alpha, argv[11], beta);

    const int n = Nx * Ny * Nz;
    const int n_k = Nx * Ny * (Nz / 2 + 1) * 2;

    float * m = (float*)malloc(n * sizeof(float));
    float * m_new = (float*)malloc(n * sizeof(float));
    float * m3 = (float*)malloc(n * sizeof(float));
    float * m3_new = (float*)malloc(n * sizeof(float));

    float * m_k = (float*)malloc(n_k * sizeof(float));
    float * m_new_k = (float*)malloc(n_k * sizeof(float));
    float * m3_k = (float*)malloc(n_k * sizeof(float));
    float * m3_new_k = (float*)malloc(n_k * sizeof(float));

    float * rand_normal = (float*)malloc(n_k / 2 * sizeof(float));
    float * rand_uniform = (float*)malloc(n_k / 2 * sizeof(float));

    void * stream = acc_get_cuda_stream(acc_async_sync);
    initCURAND(stream);
    initCUFFT(Nx, Ny, Nz, stream);

    srand(time(NULL));

    for(int i = 0; i < n; i++)
    {
        m[i] = 0;
        // printf("%.3e\n", m[i]);
    }

    #pragma acc enter data copyin(m[0:n],m_new[0:n],m3[0:n],m3_new[0:n],m_k[0:n_k],m_new_k[0:n_k],m3_k[0:n_k],m3_new_k[0:n_k],rand_normal[0:n_k/2],rand_uniform[0:n_k/2])

    int newton_avg = 0;
    float dev_avg = 0;
    float dev_alt_avg = 0;

    for(int t = 0; t < steps; t++)
    {
        if(t % dt_ana == 0)
        {
            printf("t=%d; newton_avg=%f dev_avg=%.3e dev_alt_avg=%.3e\n", t, newton_avg / (float) dt_ana, dev_avg / dt_ana, dev_alt_avg / dt_ana);
            newton_avg = 0;
            dev_avg = 0;

            char fname[1024];
            sprintf(fname, "%s_m_%lg.dat", argv[11], t * dt);
            FILE *f = fopen(fname, "w");

            #pragma acc update host(m[0:n])

            for(int i = 0; i < n; i ++)
            {
                fprintf(f, "%g ", m[i]);
            }

            fclose(f);
        }


        // initial guess for new m; calculate m3
        #pragma acc parallel loop
        for(int i = 0; i < n; i++)
        {
            m_new[i] = m[i];
            m3[i] = m[i] * m[i] * m[i];
        }

        // calculate transforms of m, m3; calculate random numbers because they should not change within Newton loop
        #pragma acc host_data use_device(m,m_k,m3,m3_k,rand_normal,rand_uniform)
        {
            launchCUFFT(m, m_k, CUFFT_R2C);
            launchCUFFT(m3, m3_k, CUFFT_R2C);
            launchCURAND_normal(rand_normal, n_k / 2);
            launchCURAND_uniform(rand_uniform, n_k / 2);
        }

        // Newton loop
        float dev_old = n;
        float dev = 0;
        int newton;
        float delta_dev = dev - dev_old;
        float dev_alt = 0;

        for(newton = 0; newton < newton_max && (delta_dev < 0 || dev > newton_dev); newton++)
        {
            // calculate m3_new
            #pragma acc parallel loop
            for(int i = 0; i < n;  i++)
            {
                m3_new[i] = m_new[i] * m_new[i] * m_new[i];
            }

            // calculate transforms of m_new, m3_new
            #pragma acc host_data use_device(m_new,m_new_k,m3_new,m3_new_k)
            {
                launchCUFFT(m_new, m_new_k, CUFFT_R2C);
                launchCUFFT(m3_new, m3_new_k, CUFFT_R2C);
            }

            // actual newton step
            dev = 0.;
            dev_alt = 0;
            #pragma acc parallel loop collapse(3) reduction(max:dev) reduction(+:dev_alt)
            for(int x = 0; x < Nx; x++)
            {
                for(int y = 0; y < Ny; y++)
                {
                    for(int z = 0; z < Nz / 2 + 1; z++)
                    {
                        const float kx = calc_freq(x, Nx, Lx);
                        const float ky = calc_freq(y, Ny, Ly);
                        const float kz = calc_freq(z, Nz, Lz);
                        const float k2 = kx * kx + ky * ky + kz * kz;

                        int i = index(x, y, z, Nx, Ny, Nz);

                        const float xi = sqrt(2 * beta * k2 / dt) * rand_normal[i];
                        float phase;

                        if((x == 0 || (Nx % 2 == 0 && x == Nx / 2)) && (y == 0 || (Ny % 2 == 0 && y == Ny / 2)) && (z == 0 || (Nz % 2 == 0 && z == Nz / 2)))
                        {
                            phase = 0;
                        }
                        else
                        {
                            phase = 2 * M_PI * rand_uniform[i];
                        }

                        i *= 2;

                        const float rhs0 = 0.5 * (m_new_k[i] * (-alpha + k2 - k2 * k2) - k2 * m3_new_k[i] + m_k[i] * (-alpha + k2 - k2 * k2) - k2 * m3_k[i]) - (m_new_k[i] - m_k[i]) / dt + xi * cos(phase);
                        const float rhs1 = 0.5 * (m_new_k[i + 1] * (-alpha + k2 - k2 * k2) - k2 * m3_new_k[i + 1] + m_k[i + 1] * (-alpha + k2 - k2 * k2) - k2 * m3_k[i + 1]) - (m_new_k[i + 1] - m_k[i + 1]) / dt + xi * sin(phase);
                        m_new_k[i] -= rhs0 / (0.5 * (-alpha + k2 - k2 * k2) - 1. / dt);
                        m_new_k[i + 1] -= rhs1 / (0.5 * (-alpha + k2 - k2 * k2) - 1. / dt);
                        const float deviation = fabsf(rhs0) + fabsf(rhs1);

                        if(deviation > dev)
                        {
                            dev = deviation;
                        }

                        dev_alt += deviation;
                    }
                }
            }

            dev /= n;
            delta_dev = dev - dev_old;
            dev_old = dev;

            dev_alt /= n;

            // inverse transform for m_new
            #pragma acc host_data use_device(m_new,m_new_k)
            {
                launchCUFFT(m_new_k, m_new, CUFFT_C2R);
            }

            // need to normalize
            #pragma acc parallel loop
            for(int i = 0; i < n; i++)
            {
                m_new[i] /= n;
            }
        }

        if(newton == newton_max)
        {
            printf("newton not converged @ t = %d (dev = %.3e)\n", t, dev);
            return -1;
        }

        newton_avg += newton;
        dev_avg += dev;
        dev_alt_avg += dev_alt / n;

        //move m_new to m
        #pragma acc parallel loop
        for(int i = 0; i < n; i ++)
        {
            m[i] = m_new[i];
        }
    }

    #pragma acc exit data copyout(m[0:n],m_new[0:n],m3[0:n],m3_new[0:n],m_k[0:n_k],m_new_k[0:n_k],m3_k[0:n_k],m3_new_k[0:n_k],rand_normal[0:n_k/2],rand_uniform[0:n_k/2])

    free(m);
    free(m_new);
    free(m3);
    free(m3_new);
    free(m_k);
    free(m_new_k);
    free(m3_k);
    free(m3_new_k);
    free(rand_normal);
    free(rand_uniform);

    cleanCURAND();
    cleanCUFFT();

    return 0;
}
